'use strict';
function initMap() {
  var mapProp = {
    center: new google.maps.LatLng(55.781603, 37.711332),
    zoom: 15,
    disableDefaultUI: true,
  };
  var map = new google.maps.Map(document.getElementById("map"), mapProp);
  var coordinates = {lat: 55.781603, lng: 37.711332};
  var image = 'img/marker-map.svg';
  var marker = new google.maps.Marker({
    position: coordinates,
    map: map,
    icon: image,
  });
  var noPoi = [
    {
      featureType: "poi",
      stylers: [
        { visibility: "off" }
      ]
    }
  ];

  map.setOptions({styles: noPoi});
}
